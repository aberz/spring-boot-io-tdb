package com.iotdb.zjc.demo;

import com.iotdb.zjc.demo.kafka.KafkaConsumer;
import com.iotdb.zjc.demo.kafka.KafkaProducer;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Set;

@SpringBootTest
class IoTdbApplicationTests {

    @Autowired
    KafkaProducer kafkaProducer;
    @Autowired
    KafkaConsumer kafkaConsumer;
    @Autowired
    DataSource dataSource;
    @Autowired
    AdminClient adminClient;
    @Value("${spring.datasource.username}")
    String username;

    @Test
    void contextLoads() throws SQLException {
        System.out.println("获取的数据库连接为:" + dataSource.getConnection() + "\n\t数据库用户名为：" + username);
    }

    @Test
    public void testKafka() {
        try {
            for (int i = 0; i < 100; i++) {
                ///消息发送
                kafkaProducer.sendMesage("TopicA", "TopicA data:" + i);
                Thread.sleep(1000L);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAllTopic() throws Exception {
        ListTopicsResult listTopics = adminClient.listTopics();
        Set<String> topics = listTopics.names().get();
        for (String topic : topics) {
            System.err.println(topic);

        }
    }


    @Test//自定义手动创建topic和分区
    public void testCreateTopic() throws InterruptedException {
        // 这种是手动创建 //10个分区，一个副本
        // 分区多的好处是能快速的处理并发量，但是也要根据机器的配置
        NewTopic topic = new NewTopic("topicB", 3, (short) 1);
        adminClient.createTopics(Arrays.asList(topic));
        Thread.sleep(1000);
    }

}
