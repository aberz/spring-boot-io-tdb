package com.iotdb.zjc.demo.mapper;

import com.iotdb.zjc.demo.data.OrderInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: zjc
 * @ClassName OrderMapper
 * @Description TODO
 * @date 2021/11/24 10:09
 * @Version 1.0
 */
public interface OrderMapper {
    Integer createOrder(OrderInfo orderInfo);

    List<OrderInfo> queryOrder();

    Integer createOrderGroup();

    Integer createOrderGroupElement();

    List<String> selectStorageGroup();

    Integer updateOrder(OrderInfo orderInfo);

    Integer deleteOrder(@Param("timestamp") String timestamp);
}
