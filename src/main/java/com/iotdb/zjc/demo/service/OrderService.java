package com.iotdb.zjc.demo.service;

import com.iotdb.zjc.demo.data.OrderInfo;

import java.util.List;

/**
 * @Author: zjc
 * @ClassName OrderService
 * @Description TODO
 * @date 2021/11/23 17:31
 * @Version 1.0
 */
public interface OrderService {
    Integer createOrder(OrderInfo orderInfo);

    Integer createOrderGroup();

    List<OrderInfo> queryOrder();

    List<String> selectStorageGroup();

    Integer updateOrder(OrderInfo orderInfo);

    Integer deleteOrder(String timestamp);
}
