package com.iotdb.zjc.demo.data;

import lombok.Data;

import java.util.Date;

/**
 * @Author: zjc
 * @ClassName OrderInfo
 * @Description TODO
 * @date 2021/11/23 17:22
 * @Version 1.0
 */
@Data
public class OrderInfo {
    public long timestamp;
    public long orderId;
    public Integer orderNum;
    public String orderName;
    public Date createTime;


    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
