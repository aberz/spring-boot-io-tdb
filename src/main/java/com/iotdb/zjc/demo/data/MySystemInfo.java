package com.iotdb.zjc.demo.data;

import lombok.Data;

/**
 * @Author stormbroken
 * Create by 2021/07/07
 * @Version 1.0
 **/

public class MySystemInfo {
    Double memoryUsage;
    Double cpuUsage;
    Double diskUsage;
    Long time;

    public Double getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(Double memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    public Double getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(Double cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public Double getDiskUsage() {
        return diskUsage;
    }

    public void setDiskUsage(Double diskUsage) {
        this.diskUsage = diskUsage;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
