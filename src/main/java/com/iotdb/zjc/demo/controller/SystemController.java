package com.iotdb.zjc.demo.controller;

import com.iotdb.zjc.demo.service.SystemService;
import com.iotdb.zjc.demo.constant.MyResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author stormbroken
 * Create by 2021/07/07
 * @Version 1.0
 **/

@RestController
@RequestMapping("/system")
public class SystemController {
    @Autowired
    SystemService systemService;

    @PostMapping("/init")
    public MyResponse init() throws Exception{
        return systemService.init();
    }

    @GetMapping("/findAll")
    public MyResponse findAll() throws Exception{
        return systemService.findAll();
    }

    @PostMapping("/delete")
    public MyResponse delete() throws Exception{
        return systemService.delete();
    }
}
