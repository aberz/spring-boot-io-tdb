package com.iotdb.zjc.demo.controller;

import com.iotdb.zjc.demo.service.OrderService;
import com.iotdb.zjc.demo.constant.MyResponse;
import com.iotdb.zjc.demo.data.OrderInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @Author: zjc
 * @ClassName OrderController
 * @Description TODO
 * @date 2021/11/23 17:16
 * @Version 1.0
 */
@RestController
public class OrderController {
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/createOrder", method = RequestMethod.POST)
//    @Transactional(rollbackFor = Exception.class)
    public MyResponse createOrder(@RequestBody OrderInfo orderInfo) {
        try {
            orderInfo.setTimestamp(System.currentTimeMillis());
            orderInfo.setCreateTime(new Date());
            Integer v = orderService.createOrder(orderInfo);
            if (v == -1) {
                return MyResponse.ok(v);
            } else {
                return MyResponse.checkForbidden(false);
            }
        } catch (Exception e) {
            log.info("创建订单失败！" + e);
            return new MyResponse(false);
        }
    }


    /**
     * 更新操作 其实也是插入操作 时间戳相同 只和时间戳相关
     * @param orderInfo
     * @return
     */
    @RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
    public MyResponse updateOrder(@RequestBody OrderInfo orderInfo) {
        try {
            orderInfo.setTimestamp(System.currentTimeMillis());
            orderInfo.setCreateTime(new Date());
            Integer v = orderService.updateOrder(orderInfo);
            if (v == -1) {
                return MyResponse.ok(v);
            } else {
                return MyResponse.checkForbidden(false);
            }
        } catch (Exception e) {
            log.info("更新订单失败！" + e);
            return new MyResponse(false);
        }
    }


    /**
     * 删除操作  要将时间戳的加号变成%2B
     * @param timestamp
     * @return
     */
    @RequestMapping(value = "/deleteOrder", method = RequestMethod.GET)
    public MyResponse deleteOrder(String timestamp) {
        try {
            Integer v = orderService.deleteOrder(timestamp);
            if (v == -1) {
                return MyResponse.ok(v);
            } else {
                return MyResponse.checkForbidden(false);
            }
        } catch (Exception e) {
            log.info("删除订单失败！" + e);
            return new MyResponse(false);
        }
    }


    /**
     * 创建组 也就是相当于数据库
     * @return
     */
    @RequestMapping(value = "/createOrderGroup", method = RequestMethod.POST)
    public MyResponse createOrderGroup() {
        try {
            Integer v = orderService.createOrderGroup();
            if (v > 0) {
                return new MyResponse(v);
            } else {
                return new MyResponse(false);
            }
        } catch (Exception e) {
            log.info("创建订单组失败！" + e);
            return new MyResponse(false);
        }
    }

    /**
     * 查询所有订单数据
     * @return
     */
    @RequestMapping(value = "/queryOrder", method = RequestMethod.GET)
    public MyResponse queryOrder() {
        try {
            List<OrderInfo> v = orderService.queryOrder();
            if (v.size() > 0) {
                v.forEach(x -> {
                    System.out.println(x.toString());
                });
                return MyResponse.ok(v);
            } else {
                return new MyResponse(false);
            }
        } catch (Exception e) {
            log.info("查询订单组失败！" + e);
            return new MyResponse(false);
        }
    }

    /**
     * 查看数据库有多少组
     *
     * @return
     */
    @RequestMapping(value = "/selectStorageGroup", method = RequestMethod.GET)
    public MyResponse selectStorageGroup() {
        try {
            List<String> v = orderService.selectStorageGroup();
            if (v.size() > 0) {
                v.forEach(x -> {
                    System.out.println("group------------------" + x.toString());
                });
                return MyResponse.ok(v);
            } else {
                return new MyResponse(false);
            }
        } catch (Exception e) {
            log.info("查询组失败！" + e);
            return new MyResponse(false);
        }
    }




}
