package com.iotdb.zjc.demo.serviceimpl;

import com.iotdb.zjc.demo.service.OrderService;
import com.iotdb.zjc.demo.data.OrderInfo;
import com.iotdb.zjc.demo.mapper.OrderMapper;
import com.iotdb.zjc.demo.util.IotDbConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: zjc
 * @ClassName OrderServiceImpl
 * @Description TODO
 * @date 2021/11/23 17:32
 * @Version 1.0
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    IotDbConfig iotDbConfig;
    @Autowired
    OrderMapper orderMapper;

    @Override
    public Integer createOrder(OrderInfo orderInfo) {
        return orderMapper.createOrder(orderInfo);
    }
    @Override
    public Integer updateOrder(OrderInfo orderInfo) {
        return orderMapper.updateOrder(orderInfo);
    }

    @Override
    public Integer createOrderGroup() {
        try {
//            List<String> statements = new ArrayList<>();
//            statements.add("set storage group to root.order");
//            statements.add("CREATE TIMESERIES root.order.orderdetail.order_id WITH DATATYPE=INT64, ENCODING=PLAIN, COMPRESSOR=SNAPPY");
//            statements.add("CREATE TIMESERIES root.order.orderdetail.order_num WITH DATATYPE=INT32, ENCODING=PLAIN, COMPRESSOR=SNAPPY");
//            statements.add("CREATE TIMESERIES root.order.orderdetail.order_name WITH DATATYPE=TEXT, ENCODING=PLAIN, COMPRESSOR=SNAPPY");
//            statements.add("CREATE TIMESERIES root.order.orderdetail.create_time WITH DATATYPE=TEXT, ENCODING=PLAIN, COMPRESSOR=SNAPPY");
//            Integer flag = iotDbConfig.executeBatch(statements);
            Integer flag = orderMapper.createOrderGroup();
            Integer flagEle = orderMapper.createOrderGroupElement();
            System.out.println("\n\t执行sql数量为{}" + flagEle);
            return flagEle;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<OrderInfo> queryOrder() {
        return orderMapper.queryOrder();
    }

    @Override
    public List<String> selectStorageGroup() {
        return orderMapper.selectStorageGroup();
    }

    @Override
    public Integer deleteOrder(String timestamp) {
        return orderMapper.deleteOrder(timestamp);
    }
}
