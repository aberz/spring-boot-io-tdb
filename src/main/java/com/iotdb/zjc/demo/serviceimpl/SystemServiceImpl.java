package com.iotdb.zjc.demo.serviceimpl;

import com.iotdb.zjc.demo.service.SystemService;
import com.iotdb.zjc.demo.constant.MyResponse;
import com.iotdb.zjc.demo.data.MySystemInfo;
import com.iotdb.zjc.demo.util.IotDbConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * TODO 资源管理需要再斟酌，提前关闭导致无法读出数据
 *
 * @Author stormbroken
 * Create by 2021/07/07
 * @Version 1.0
 **/

@Service
public class SystemServiceImpl implements SystemService {
    private static final String INSERT = "insert into root.demo1.pc1(timestamp, memory, cpu, disk) values(?,?,?,?)";
    @Autowired
    IotDbConfig iotDbConfig;

    /**
     * 初始化系统服务
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Override
    public MyResponse init() throws ClassNotFoundException, SQLException {
        List<String> statements = new ArrayList<>();
        statements.add("set storage group to root.demo1");
        statements.add("create timeseries root.demo1.pc1.memory WITH datatype=FLOAT,encoding=RLE");
        statements.add("create timeseries root.demo1.pc1.cpu WITH datatype=FLOAT,encoding=RLE");
        statements.add("create timeseries root.demo1.pc1.disk WITH datatype=FLOAT,encoding=RLE");
        iotDbConfig.executeBatch(statements);
        return MyResponse.ok("创建成功");
    }

    /**
     * 插入一条系统数据
     *
     * @param mySystemInfo
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Override
    public boolean insert(MySystemInfo mySystemInfo) throws SQLException {
        PreparedStatement preparedStatement = iotDbConfig.getConnection().prepareStatement(INSERT);
        preparedStatement.setLong(1, mySystemInfo.getTime());
        preparedStatement.setDouble(2, mySystemInfo.getMemoryUsage());
        preparedStatement.setDouble(3, mySystemInfo.getCpuUsage());
        preparedStatement.setDouble(4, mySystemInfo.getDiskUsage());
        return preparedStatement.execute();
    }

    /**
     * 查询全部数据
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Override
    public MyResponse findAll() {
        List<Map<String, Object>> resultSet = iotDbConfig.executeQuery("select * from root.demo");
        return MyResponse.ok(resultSet);
    }

    /**
     * 删除所有数据
     *
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Override
    public MyResponse delete() {
        return iotDbConfig.execute("DELETE STORAGE GROUP root.demo") ? MyResponse.ok("删除成功") : MyResponse.error("删除失败");
    }

    /**
     * 将结果集转换为系统参数
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private List<MySystemInfo> set2Result(ResultSet resultSet) throws SQLException {
        List<MySystemInfo> mySystemInfos = new ArrayList<>();
        if (resultSet != null) {
            while (resultSet.next()) {
                MySystemInfo mySystemInfo = new MySystemInfo();
                mySystemInfo.setTime(Long.valueOf(resultSet.getString(1)));
                mySystemInfo.setDiskUsage(Double.valueOf(resultSet.getString(2)));
                mySystemInfo.setMemoryUsage(Double.valueOf(resultSet.getString(3)));
                mySystemInfo.setCpuUsage(Double.valueOf(resultSet.getString(4)));

                mySystemInfos.add(mySystemInfo);
            }
        }
        return mySystemInfos;
    }
}
